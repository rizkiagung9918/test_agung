import bs4
import requests
import datetime
import datetime
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from fastapi import FastAPI
from pymongo import MongoClient
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score


headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
    'Accept-Language':'en-US,en;q=0.5',
    'cookie':'locale=en_US;'
}

#Data Crawling Tokopedia

url = 'https://www.klikindomaret.com/page/unilever-officialstore'
r = requests.get(url,headers=headers)
soup = bs4.BeautifulSoup(r.text,'html.parser')

def get_product(_url:str):
    
    r = requests.get(_url,headers=headers)
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    list_name = []
    list_price = []
    list_or_price = []
    list_disc = []
    list_platform = []
    list_detail = []
    #print(_url.split('/')[2])
    try:
        all_item = soup.find_all('div',{'class':'wrp-content'})
        for item in all_item:
            try:
                name = item.find('div',{'class':'title'}).text.strip() 
                detail = name.split(' ')[-1]
                or_price = item.find('div',{'class':'wrp-price'}).find('div').find('div').find('span',{'class':'strikeout disc-price'}).text.replace('Rp','').replace('.','').split(' ')[-1].strip()
                price = item.find('div',{'class':'wrp-price'}).find('div').find('div').find('span',{'class':'normal price-value'}).text.replace('Rp','').replace('.','').strip()
                disc = item.find('div',{'class':'wrp-price'}).find('div').find('div').find('span',{'class':'strikeout disc-price'}).text.replace('Rp','').replace('.','').split(' ')[0].strip()
                platform = _url.split('/')[2]
                
                list_name.append(name)
                list_detail.append(detail)
                list_price.append(price)
                list_or_price.append(or_price)
                list_disc.append(disc)
                list_platform.append(platform)
            except:
                pass
    except:
        pass
    
    dict_product = {
        'name_product' : list_name,
        'price_product' : list_price,
        'price_or_product' : list_or_price,
        'discount_product' : list_disc,
        'platform' : list_platform,
        'detail' : list_detail
    }
    
    return dict_product

data = get_product(url)


#Data Processing

uri = "mongodb://root:CzQcn8djjJa6PA@15.15.15.119:27078/"

# Connect to MongoDB
client = MongoClient(uri)

# Create crawler database
db = client.test_agung

for i in range(len(data['name_product'])):
    # Step 4: Insert data into the collection
    data_to_insert = {
        "id": i+1,
        "name": data['name_product'][i],
        "price": data['price_product'][i],
        "original price": data['price_or_product'][i],
        "discount percentage": data['discount_product'][i],
        "detail": data['detail'][i],
        "platform": data['platform'][i],
        "product masterid": i+1,
        "create date": datetime.datetime.now()
    }
    i = i + 1

    # Insert a single document
    result = db.product.insert_one(data_to_insert)

for i in range(len(data['name_product'])):
    # Step 4: Insert data into the collection
    data_to_insert = {
        "id": i+1,
        "type": data['name_product'][i].split(' ')[0],
        "name": data['name_product'][i],
        "detail": data['detail'][i],
    }
    i = i + 1

    # Insert a single document
    result = db.product_master.insert_one(data_to_insert)


#machine learning
def price_recommendation():
    X = np.array(data['price_product']).reshape(-1, 1)
    y = np.array(data['price_or_product']).reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    regression_model = LinearRegression()
    regression_model.fit(X_train, y_train)
    y_pred = regression_model.predict(X_test)
    
    return y_pred

price_recommendation = price_recommendation().min()

#API developments

app = FastAPI()

# Create a simple API route
@app.get("/")
def price_recommendation():
    price_recommendation = price_recommendation().min()
    return price_recommendation