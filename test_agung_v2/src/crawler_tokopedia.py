import bs4
import requests
import datetime
import datetime
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from fastapi import FastAPI
from pymongo import MongoClient
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score


headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
    'Accept-Language':'en-US,en;q=0.5',
    'cookie':'locale=en_US;'
}

#Data Crawling Tokopedia

url = 'https://www.tokopedia.com/unilever/product'
r = requests.get(url,headers=headers)
soup = bs4.BeautifulSoup(r.text,'html.parser')

def get_product(_url:str):
    
    r = requests.get(_url,headers=headers)
    soup = bs4.BeautifulSoup(r.text,'html.parser')
    list_name = []
    list_price = []
    list_or_price = []
    list_disc = []
    list_platform = []
    list_detail = []
    
    try:
        all_item = soup.find_all('div',{'class':'css-974ipl'})
        for item in all_item:
            try:
                name = item.find('a').find('div',{'data-testid':'linkProductName'}).text
                price = int(item.find('a').find('div',{'data-testid':'linkProductPrice'}).text.replace('Rp ','').replace('.',''))
                or_price = int(item.find('a').find('div',{'data-testid':'lblProductSlashPrice'}).text.replace('Rp ','').replace('.',''))
                disc = item.find('a').find('div',{'class':''}).find('div',{'class':'css-hwbdeb'}).find('div',{'data-testid':'lblProductDiscount'}).text.strip()
                detail = name.split(' ')[-1]
                platform = _url.split('/')[2]
                
                list_name.append(name)
                list_price.append(price)
                list_or_price.append(or_price)
                list_disc.append(disc)
                list_platform.append(platform)
                list_detail.append(detail)
            except:
                pass
    except:
        pass
    
    dict_product = {
        'name_product' : list_name,
        'price_product' : list_price,
        'price_or_product' : list_or_price,
        'discount_product' : list_disc,
        'platform' : list_platform,
        'detail': list_detail
    }
    
    return dict_product

data = get_product(url)